/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"database/sql"
	"fmt"
	"os"

	"github.com/davecgh/go-spew/spew"
	_ "github.com/go-sql-driver/mysql"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

type statusR struct {
	ID      string
	checked bool
}

// statusCmd represents the status command
var statusCmd = &cobra.Command{
	Use:   "status",
	Short: "Gets a status of the database",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("status called")
		dbs, err := GetConn()
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(dbs)
	},
}

func init() {
	rootCmd.AddCommand(statusCmd)
}
func GetConn() (*sql.DB, error) {
	viper.SetConfigName("database") // config file name without extension
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	viper.AddConfigPath("./config/") // config file path
	viper.AutomaticEnv()             // read value ENV variable

	err := viper.ReadInConfig()
	if err != nil {
		fmt.Println("fatal error config file: default \n", err)
		os.Exit(1)
	}

	// Declare var
	dialect := viper.GetString("development.dialect")
	// database := viper.GetString("development.database")
	// host := viper.GetString("development.host")
	// port := viper.GetString("development.port")
	// user := viper.GetString("development.user")
	// password := viper.GetString("development.password")
	// protocal := viper.GetString("development.protocal")
	DS := viper.GetString("development.datasource")
	// Print
	// fmt.Println("dialect :", dialect)
	// fmt.Println("database :", database)
	// fmt.Println("host :", host)
	// fmt.Println("port :", port)
	// fmt.Println("user :", user)
	// fmt.Println("password :", password)
	// fmt.Println("password :", protocal)

	// pdbcom := fmt.Sprintf("%s:%s@%s(%s:%s)/%s",
	// 	user, password, protocal, host, port, database,
	// )
	// viper.Set("database.datasource", pdbcom)
	//make a connection to the database
	fmt.Println(viper.GetString("database.datasource"))
	db, err := sql.Open(dialect, DS)
	if err != nil {
		fmt.Errorf("Cannot connect to database: %s", err)
	}
	fmt.Println(viper.GetString("database.datasource"))
	err = db.Ping()
	if err != nil {
		fmt.Println(err)
	}

	type Post struct {
		ID     string `json:"id" form:"id" query:"id"`
		Name   string `json:"name" form:"name" query:"name"`
		Email  string `json:"email" form:"email" query:"email"`
		userid string `json:"userid" form:"userid" query:"userid"`
	}

	var (
		id     string
		name   string
		email  string
		userid string
		post   []Post
	)
	i := 0
	//get from database
	rows, err := db.Query("select * from ap")
	if err != nil {
		fmt.Println(err)
	}
	for rows.Next() {
		err := rows.Scan(&id, &name, &email, &userid)
		if err != nil {
			fmt.Println(err)
		} else {
			i++
			fmt.Println("scan ", i)
		}
		p := Post{ID: id, Name: name, Email: email, userid: userid}
		post = append(post, p)

	}
	defer rows.Close()
	defer db.Close()
	spew.Dump(post)

	os.Stat(viper.GetString("database.dir"))
	if err != nil {
		fmt.Println(err)
	}

	return db, nil
}

//https://techinscribed.com/create-db-migrations-tool-in-go-from-scratch/
