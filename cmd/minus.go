/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// minusCmd represents the minus command
var minusCmd = &cobra.Command{
	Use:   "minus",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("minus called")

		viper.SetConfigName("database") // config file name without extension
		viper.SetConfigType("yaml")
		viper.AddConfigPath(".")
		viper.AddConfigPath("./config/") // config file path
		viper.AutomaticEnv()             // read value ENV variable

		err := viper.ReadInConfig()
		if err != nil {
			fmt.Println("fatal error config file: default \n", err)
			os.Exit(1)
		}

		// Declare var
		dialect := viper.GetString("development.dialect")
		database := viper.GetString("development.database")
		host := viper.GetString("development.host")
		port := viper.GetString("development.port")
		user := viper.GetString("development.user")
		password := viper.GetString("development.password")
		protocal := viper.GetString("development.protocal")

		fmt.Println("dialect :", dialect)
		fmt.Println("database :", database)
		fmt.Println("host :", host)
		fmt.Println("port :", port)
		fmt.Println("user :", user)
		fmt.Println("password :", password)
		fmt.Println("password :", protocal)

		pdbcom := fmt.Sprintf("%s:%s@%s(%s:%s)/%s",
			user, password, protocal, host, port, database,
		)
		viper.Set("database.datasource", pdbcom)
	},
}

func init() {
	rootCmd.AddCommand(minusCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// minusCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// minusCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
