module gitlab.com/zendrulat123/venom

go 1.16

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/rubenv/sql-migrate v0.0.0-20200616145509-8d140a17f351 // indirect
	github.com/scarbo87/sql-migrate v1.0.1 // indirect
	github.com/scarbo87/sql-migrate-cobra v1.0.1 // indirect
	github.com/spf13/cobra v1.1.1
	github.com/spf13/viper v1.7.1
)
