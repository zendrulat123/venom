package main

import (
	"gitlab.com/zendrulat123/venom/cmd"
)

func main() {
	cmd.Execute()
}
